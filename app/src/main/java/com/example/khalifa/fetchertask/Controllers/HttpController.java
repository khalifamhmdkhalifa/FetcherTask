package com.example.khalifa.fetchertask.Controllers;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by khalifa on 9/6/2017.
 */

public class HttpController
{
static final String BASE_URL="https://my-instagram-api.herokuapp.com";


public static Retrofit getDefaultBuilder()
{

    Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build();
    return retrofit;

}


}
