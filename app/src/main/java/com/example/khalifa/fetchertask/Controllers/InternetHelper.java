package com.example.khalifa.fetchertask.Controllers;

import android.content.Context;
import android.net.ConnectivityManager;

/**
 * Created by khalifa on 9/6/2017.
 */

public class InternetHelper
{
public static boolean isInternetConnectionAvailable(Context context)
{
    ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
    return cm.getActiveNetworkInfo() != null;
}

}
