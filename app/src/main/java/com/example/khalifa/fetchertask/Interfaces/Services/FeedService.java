package com.example.khalifa.fetchertask.Interfaces.Services;

import com.example.khalifa.fetchertask.Entities.ListItem;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by khalifa on 10/6/2017.
 */

public interface FeedService
{
    @GET("/feed")
    Call<GetFeedsResponse> getItems();


    public class GetFeedsResponse
    {
        public List<ListItem> getFeeds() {
            return data;
        }
        List<ListItem> data;

        public Meta getMeta() {
            return meta;
        }

        public void setMeta(Meta meta) {
            this.meta = meta;
        }

        Meta meta;
    public class  Meta {
        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        int code;
    }
    }
}
