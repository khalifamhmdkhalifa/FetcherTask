package com.example.khalifa.fetchertask.Entities;

import java.io.Serializable;

/**
 * Created by khalifa on 10/6/2017.
 */

public class Image implements Serializable
{
    int width;
    int height;
    String url;

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
