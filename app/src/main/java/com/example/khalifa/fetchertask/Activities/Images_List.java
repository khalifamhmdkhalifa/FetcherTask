package com.example.khalifa.fetchertask.Activities;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.example.khalifa.fetchertask.Adapters.ListItemAdapter;
import com.example.khalifa.fetchertask.Controllers.InternetHelper;
import com.example.khalifa.fetchertask.Controllers.ItemsController;
import com.example.khalifa.fetchertask.Entities.ListItem;
import com.example.khalifa.fetchertask.R;

import java.io.Serializable;
import java.util.List;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;

public class Images_List extends AppCompatActivity {


    private static final String TAG ="Images_List" ;
    Context context;

    List<ListItem> items;
    ListItemAdapter listItemAdapter;
    ItemsController itemsController;
    ItemsReceiver itemReceiver;
    @BindView(R.id.ActivityImagesList_ItemsRecyclerView)RecyclerView itemsRv;
    //@BindView(R.id.frag_container) View frag_Container;
    @BindView(R.id.ActivityImagesList_LoadingView)
    View loadingView;
    @BindString(R.string.No_Internet_Message) String noInternetMessage;
    @BindString(R.string.FAILED_TO_CONNECT_TO_SERVER) String failedToConnectToServerMessage;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_images_list);
        context = this;
        ButterKnife.bind(this);
        itemsController = new ItemsController();
        itemsRv.setLayoutManager(new LinearLayoutManager(context));
        if(InternetHelper.isInternetConnectionAvailable(this))
        {
            loadData();
        }else
        {
            loadingView.setVisibility(View.GONE);
            Toast.makeText(context,noInternetMessage,Toast.LENGTH_LONG).show();

        }

    }


    void loadData()
    {
        loadingView.setVisibility(View.VISIBLE);
        if(itemReceiver == null)
            itemReceiver = new ItemsReceiver();
        itemsController.getItemsList(itemReceiver);
        itemsRv.setAdapter(listItemAdapter);

    }


    public class ItemsReceiver  implements ItemsController.GetListItemsListener
    {


        @Override
        public void onListRecieved(List<ListItem> atheletes)
        {
            loadingView.setVisibility(View.GONE);
            items =  atheletes;
            listItemAdapter = new ListItemAdapter(items,context,new ListItemAdapter.ItemClickListener()
            {

                @Override
                public void onItemClicked(int position, ListItem item) {
                  //  replaceFragment(AtheleteDetails.newInstance(item),AtheleteDetails.class.getName(),true);

                    startDetailsActivity(item)   ;         }
            });
            itemsRv.setAdapter(listItemAdapter);
        }

        @Override
        public void onFailur(Throwable throwable) {
            loadingView.setVisibility(View.GONE);
            Toast.makeText(context,failedToConnectToServerMessage,Toast.LENGTH_LONG).show();
            throwable.printStackTrace();
            Log.e(TAG,throwable.getMessage());
           // ((TextView)loadingView).setText();
        }
    }

    private void startDetailsActivity(ListItem item)
    {
        Intent intent =new Intent(this , ItemDetailsActivity.class);
        Bundle bundle =new Bundle();
        bundle.putSerializable(ItemDetailsActivity.ItemKey, item);
        intent.putExtras(bundle);
        startActivity(intent);

    }


}
