package com.example.khalifa.fetchertask.Activities;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.example.khalifa.fetchertask.Adapters.ListItemAdapter;
import com.example.khalifa.fetchertask.Entities.ListItem;
import com.example.khalifa.fetchertask.R;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ItemDetailsActivity extends AppCompatActivity {
ListItem item;
    public static  final String ItemKey = "item";
    final DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy") ;
    final Calendar calendar = Calendar.getInstance();


    @BindView(R.id.SingleListItemView_Image) ImageView imageView;

    @BindView(R.id.SingleListItemView_UserImage) ImageView userImage;
    @BindView(R.id.SingleListItemView_LikeImage) ImageView likeImage;
   @BindView(R.id.SingleListItemView_LocationTextView) TextView loacation;
  @BindView(R.id.SingleListItemView_Text) TextView text;


    @BindView(R.id.SingleListItemView_UserName)TextView userName;


    @BindView(R.id.SingleListItemView_Date) TextView date ;
    @BindView(R.id.SingleListItemView_LikeCount) TextView likeCount;
    @BindView(R.id.SingleListItemView_CommentCount) TextView commentCount;
    @BindView(R.id.SingleListItemView_Tags) TextView tags;



















    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.item_details);
        Bundle bundle = getIntent().getExtras();
        ButterKnife.bind(this);
        if(bundle!=null && bundle.containsKey(ItemKey))
        {
            item = (ListItem) bundle.getSerializable(ItemKey);

        }
    if(item!=null)
    {
        updateScreen(item);

    }


    }

    private void updateScreen(ListItem listItem) {



        /*
        *

            likeCount = (TextView) itemView.findViewById(R.id.SingleListItemView_LikeCount);
            commentCount = (TextView) itemView.findViewById(R.id.SingleListItemView_CommentCount);
        *
        *
        * */



         userName.setText(listItem.getUser().getFull_name());
         likeCount.setText(String.valueOf(listItem.getLikes().getCount()));
         commentCount.setText(String.valueOf(listItem.getComments().getCount()));



        try {


            long milliSeconds = Long.parseLong(listItem.getCreated_time()) * 1000;
            calendar.setTimeInMillis(milliSeconds);
             date.setText(formatter.format(calendar.getTime()));
        } catch (Exception e) {
            e.printStackTrace();

        }


        if (listItem.isUser_has_liked())
             likeImage.setImageResource(R.drawable.like_active);
        else
             likeImage.setImageResource(R.drawable.like);


        Glide.with(this).load(listItem.getUser().getProfile_picture()).asBitmap().into(new BitmapImageViewTarget( userImage) {
            @Override
            public void onLoadFailed(Exception e, Drawable errorDrawable) {
                super.onLoadFailed(e, errorDrawable);
                if(e!=null)
                    e.printStackTrace();
                 userImage.setImageResource(R.mipmap.ic_launcher);
            }

            @Override
            protected void setResource(Bitmap resource) {
                RoundedBitmapDrawable circularBitmapDrawable =
                        RoundedBitmapDrawableFactory.create(getResources(), resource);
                circularBitmapDrawable.setCircular(true);
                 userImage.setImageDrawable(circularBitmapDrawable);
            }

        });

        if (listItem.getLocation() == null) {
             loacation.setVisibility(View.INVISIBLE);


        } else {
             loacation.setText(listItem.getLocation().getName());

        }


         text.setText(listItem.getCaption().getText());


        Glide.with(this).load(listItem.getImages().getStandard_resolution().getUrl()).asBitmap().into(new BitmapImageViewTarget( imageView) {
            @Override
            public void onLoadFailed(Exception e, Drawable errorDrawable) {
                super.onLoadFailed(e, errorDrawable);
                e.printStackTrace();
                 imageView.setImageResource(R.mipmap.ic_launcher);
            }

        });





    if(item.getTags()!=null)
    {
        StringBuilder sb =new StringBuilder(getResources().getString(R.string.TAGS) + ": ");
        for( String string : item.getTags() )
        {

            sb.append("#"+string+" ");
        }
        tags.setText(sb.toString());
    }else
        tags.setVisibility(View.GONE);
    }
}
