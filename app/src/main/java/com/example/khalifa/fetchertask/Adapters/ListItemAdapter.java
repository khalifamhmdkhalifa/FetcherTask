package com.example.khalifa.fetchertask.Adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.example.khalifa.fetchertask.Entities.ListItem;
import com.example.khalifa.fetchertask.R;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

/**
 * Created by khalifa on 9/6/2017.
 */

public class ListItemAdapter extends RecyclerView.Adapter {
    List<ListItem> atheletes;
    Context context;
    ItemClickListener itemClickListener;
     final DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy") ;
    final Calendar calendar = Calendar.getInstance();



    public ListItemAdapter(List<ListItem> atheletes, Context context, ItemClickListener itemClickListener) {
        this.atheletes = atheletes;
        this.context = context;
        this.itemClickListener = itemClickListener;
    }




    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new CustomeViewHolder(LayoutInflater.from(context).inflate(R.layout.single_list_item_view, parent, false));
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position)
    {



            final ListItem listItem = atheletes.get(position);
            ((CustomeViewHolder) holder).root.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (itemClickListener != null)
                        itemClickListener.onItemClicked(position, listItem);
                }
            });

        /*
        *

            likeCount = (TextView) itemView.findViewById(R.id.SingleListItemView_LikeCount);
            commentCount = (TextView) itemView.findViewById(R.id.SingleListItemView_CommentCount);
        *
        *
        * */



            ((CustomeViewHolder) holder).userName.setText(listItem.getUser().getFull_name());
            ((CustomeViewHolder) holder).likeCount.setText(String.valueOf(listItem.getLikes().getCount()));
            ((CustomeViewHolder) holder).commentCount.setText(String.valueOf(listItem.getComments().getCount()));



            try {


                long milliSeconds = Long.parseLong(listItem.getCreated_time()) * 1000;
                calendar.setTimeInMillis(milliSeconds);
                ((CustomeViewHolder) holder).date.setText(formatter.format(calendar.getTime()));
            } catch (Exception e) {
                e.printStackTrace();

            }


            if (listItem.isUser_has_liked())
                ((CustomeViewHolder) holder).likeImage.setImageResource(R.drawable.like_active);
            else
                ((CustomeViewHolder) holder).likeImage.setImageResource(R.drawable.like);


            Glide.with(context).load(listItem.getUser().getProfile_picture()).asBitmap().into(new BitmapImageViewTarget(((CustomeViewHolder) holder).userImage) {
                @Override
                public void onLoadFailed(Exception e, Drawable errorDrawable) {
                    super.onLoadFailed(e, errorDrawable);
                  if(e!=null)
                    e.printStackTrace();
                    ((CustomeViewHolder) holder).userImage.setImageResource(R.mipmap.ic_launcher);
                }

                @Override
                protected void setResource(Bitmap resource) {
                    RoundedBitmapDrawable circularBitmapDrawable =
                            RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                    circularBitmapDrawable.setCircular(true);
                    ((CustomeViewHolder) holder).userImage.setImageDrawable(circularBitmapDrawable);
                }

            });

            if (listItem.getLocation() == null) {
                ((CustomeViewHolder) holder).loacation.setVisibility(View.INVISIBLE);


            } else {
                ((CustomeViewHolder) holder).loacation.setText(listItem.getLocation().getName());

            }


            ((CustomeViewHolder) holder).text.setText(listItem.getCaption().getText());


            Glide.with(context).load(listItem.getImages().getStandard_resolution().getUrl()).asBitmap().into(new BitmapImageViewTarget(((CustomeViewHolder) holder).imageView) {
                @Override
                public void onLoadFailed(Exception e, Drawable errorDrawable) {
                    super.onLoadFailed(e, errorDrawable);
                    e.printStackTrace();
                    ((CustomeViewHolder) holder).imageView.setImageResource(R.mipmap.ic_launcher);
                }

            });



    }

    @Override
    public int getItemCount()
    {
        return atheletes.size();
    }



    class CustomeViewHolder extends RecyclerView.ViewHolder
    {

        ImageView imageView,userImage;
        ImageView likeImage;
        TextView loacation;
        TextView text;


        TextView userName,date,likeCount,commentCount;
        View root;


        public CustomeViewHolder(View itemView)
        {
            super(itemView);
            this.root = itemView;
            imageView = (ImageView) itemView.findViewById(R.id.SingleListItemView_Image);
            userImage = (ImageView) itemView.findViewById(R.id.SingleListItemView_UserImage);
            likeImage = (ImageView) itemView.findViewById(R.id.SingleListItemView_LikeImage);
            loacation = (TextView) itemView.findViewById(R.id.SingleListItemView_LocationTextView);
            text = (TextView) itemView.findViewById(R.id.SingleListItemView_Text);
            userName = (TextView) itemView.findViewById(R.id.SingleListItemView_UserName);
            date = (TextView) itemView.findViewById(R.id.SingleListItemView_Date);
            likeCount = (TextView) itemView.findViewById(R.id.SingleListItemView_LikeCount);
            commentCount = (TextView) itemView.findViewById(R.id.SingleListItemView_CommentCount);


            // imageView = (ImageView) itemView.findViewById(R.id.single_athelete_item_image);
            //name = (TextView) itemView.findViewById(R.id.single_athelete_item_name);
            //ButterKnife.bind(itemView);
        }
    }


    public interface ItemClickListener
    {
     public void onItemClicked(int position, ListItem item);
    }
}
