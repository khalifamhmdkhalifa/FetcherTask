package com.example.khalifa.fetchertask.Controllers;

import com.example.khalifa.fetchertask.Entities.ListItem;
import com.example.khalifa.fetchertask.Interfaces.Services.FeedService;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by khalifa on 10/6/2017.
 */

public class ItemsController
{


public void  getItemsList(final GetListItemsListener listener)
{
    Retrofit retrofit =  HttpController.getDefaultBuilder();
    FeedService feedService = retrofit.create(FeedService.class);
    Call<FeedService.GetFeedsResponse> call  = feedService.getItems();

    call.enqueue(new Callback<FeedService.GetFeedsResponse>()
    {
        @Override
        public void onResponse(Call<FeedService.GetFeedsResponse> call, Response<FeedService.GetFeedsResponse> response)
        {
            if(response.isSuccessful() ) //response.body().getMeta().getCode() == 200)
            listener.onListRecieved(response.body().getFeeds());
            else
            listener.onFailur(new Throwable("failed"));

        }

        @Override
        public void onFailure(Call<FeedService.GetFeedsResponse> call, Throwable t)
        {
            listener.onFailur(t);
        }
    });

   // return  call;

}





    public  interface GetListItemsListener
    {
        void onListRecieved(List<ListItem> items);
        void onFailur(Throwable throwable);
    }


}
