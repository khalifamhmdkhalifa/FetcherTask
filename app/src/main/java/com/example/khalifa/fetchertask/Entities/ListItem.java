package com.example.khalifa.fetchertask.Entities;

import java.io.Serializable;
import java.util.List;

public class ListItem implements Serializable
{
    String id;
    User user;
    Images images;
    String created_time;
    Caption caption;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Images getImages() {
        return images;
    }

    public void setImages(Images images) {
        this.images = images;
    }

    public String getCreated_time() {
        return created_time;
    }

    public void setCreated_time(String created_time) {
        this.created_time = created_time;
    }

    public Caption getCaption() {
        return caption;
    }

    public void setCaption(Caption caption) {
        this.caption = caption;
    }

    public boolean isUser_has_liked() {
        return user_has_liked;
    }

    public void setUser_has_liked(boolean user_has_liked) {
        this.user_has_liked = user_has_liked;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public Comment getComments() {
        return comments;
    }

    public void setComments(Comment comments) {
        this.comments = comments;
    }

    public String getFilter() {
        return filter;
    }

    public void setFilter(String filter) {
        this.filter = filter;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    boolean user_has_liked;
    List<String> tags;
    Comment comments;

    public Like getLikes() {
        return likes;
    }

    public void setLikes(Like likes) {
        this.likes = likes;
    }

    Like likes;
    String filter;
    String type;
    String link;
    Location location;




public class Images implements Serializable  {

    Image thumbnail;
    Image low_resolution;
    Image standard_resolution;

    public Image getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(Image thumbnail) {
        this.thumbnail = thumbnail;
    }

    public Image getLow_resolution() {
        return low_resolution;
    }

    public void setLow_resolution(Image low_resolution) {
        this.low_resolution = low_resolution;
    }

    public Image getStandard_resolution() {
        return standard_resolution;
    }

    public void setStandard_resolution(Image standard_resolution) {
        this.standard_resolution = standard_resolution;
    }
}

public class Like implements Serializable {
    int count;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
public class Comment implements Serializable
{
    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    int count;
}


}